package sample;

import javafx.fxml.FXML;
import javafx.scene.control.*;

public class Controller {
    public MenuItem save;
    public MenuItem delete;
    public MenuItem preview;
    public MenuItem objectMenu;

    public MenuItem terrainMenu;

    public void save(){
        Boxes.displayConfirmBox("save","Do you want to save this XML file");
    }

    public void previewXML(){}

    public void delete(){}

    public void showObjectMenu(){}

     public void showTerrainMenu(){}
}
