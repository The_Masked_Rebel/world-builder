package sample;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicInteger;

public class Boxes {
    public static final int CONFIRM_YES = 1;
    public static final int CONFIRM_NO = 2;
    public static final int CONFIRM_CANCEL = 0;

    public static int displayConfirmBox(String title,String message){
        Stage window = new Stage();
        AtomicInteger answer = new AtomicInteger(CONFIRM_CANCEL);

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        GridPane pane = new GridPane();
        pane.setVgap(4);
        pane.setHgap(4);
        pane.setPadding(new Insets(8,8,8,8));

        Label label = new Label(message);
        GridPane.setConstraints(label,0,0);

        Button yes = new Button("Yes");
        yes.setOnAction(e ->{
            answer.set(CONFIRM_YES);
            window.close();
        });
        GridPane.setConstraints(yes,0,2);

        Button no = new Button("No");
        no.setOnAction(e -> {
            answer.set(CONFIRM_NO);
            window.close();
        });
        GridPane.setConstraints(no,1,2);


        Button cancel = new Button("Cancel");
        cancel.setOnAction(e -> window.close());
        GridPane.setConstraints(cancel,2,2);

        pane.getChildren().addAll(label,yes,no,cancel);
        window.setScene(new Scene(pane, 250,100));
        window.showAndWait();

        return answer.get();
    }
}
